prefixes:
  schema: 'https://schema.org/'
  xsd: 'http://www.w3.org/2001/XMLSchema#'

mappings:
  tvshows:
    sources:
      - ['data/shows.json~jsonpath', '$.[*]']
    s: https://dylanvanassche.be/tvshows/$(id)
    po:
      - [a, schema:TVSeries]
      - [schema:name, $(seriesName), xsd:string]
      - [schema:image, https://dylanvanassche.be/tvshows/${id}/$(poster), xsd:anyURI]
      - [schema:image, https://dylanvanassche.be/tvshows/${id}/$(banner), xsd:anyURI]
      - [schema:url, https://www.thetvdb.com/?tab=series&id=$(id), xsd:anyURI]
      - [schema:startDate, $(firstAired), xsd:date]
      - [schema:abstract, $(overview), xsd:string]
      - [schema:dateModified, $(lastUpdated), xsd:integer]
      - [schema:numberOfSeasons, $(season), xsd:integer]
      - [schema:aggregateRating, $(siteRating), xsd:decimal]
      - [schema:contentRating, $(rating), xsd:string]
      - [schema:creativeWorkStatus, $(status), xsd:string]
      - [schema:publisher, $(network), xsd:string]
      - [schema:genre, $(genre), xsd:string]
      - [schema:timeRequired, PT$(runtime)M, xsd:duration]
      - [schema:inLanguage, $(language), xsd:string]
      - [schema:sameAs, https://imdb.com/title/$(imdbId), xsd:anyURI]
      - p: schema:epsiode
        o:
         - mapping: episodes
           condition:
             function: equal
             parameters:
               - [str1, $(id)]
               - [str2, $(seriesId)]
      
  episodes:
    sources:
      - ['data/shows.json~jsonpath', '$.[*].episodes.[*]']
    s: https://dylanvanassche.be/tvshows/$(seriesId)/episode/$(id)
    po:
      - [a, schema:Episode]
      - [schema:name, $(episodeName), xsd:string]
      - [schema:about, $(overview), xsd:string]
      - [schema:datePublished, $(firstAired), xsd:date]
      - [schema:dateModified, $(lastUpdated), xsd:integer]
      - [schema:episodeNumber, $(airedEpisodeNumber), xsd:integer]
      - [schema:partOfSeason, $(airedSeason), xsd:integer]
      - [schema:sameAs, https://imdb.com/title/$(imdbId), xsd:anyURI]
      - [schema:url, https://thetvdb.com/series/$(seriesId)/episodes/$(id), xsd:anyURI]
      - [schema:aggregateRating, $(siteRating), xsd:decimal]
      - [schema:contentRating, $(contentRating), xsd:string]
      
