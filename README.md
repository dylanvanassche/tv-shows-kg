# Knowledge Graph of my TV show library

## Build

- `cd fetcher`
- `pipenv install`
- `pipenv shell`
- `./fetcher.py ../shows.json --username $USERNAME --user-key $USER_KEY --api-key $API_KEY --mapping-file ../mappings/mappings.rml.ttl --jar ../rmlmapper-4.7.0-r152.jar`

## License

Available under the GPLv3 license.
&copy; Dylan Van Assche (2020)
