#!/usr/bin/env python

import os
import json
import argparse
from pathlib import Path

import requests
from tvdb_api_client import TVDBClient

from mapper import Mapper

DATA_DIR = './data'
IMAGE_BASE_URL = 'https://artworks.thetvdb.com/banners'

class Fetcher(TVDBClient):
    def __init__(self, username, user_key, api_key, cache=None):
        super().__init__(username, user_key, api_key, cache)

        # List of shows
        self.shows = []

        # Create directories
        Path(f'{DATA_DIR}').mkdir(parents=True, exist_ok=True)

    def save_show(self, show_id):
        # Fetch show information
        show = self.get_series_by_id(show_id)

        # Fetch episode information
        show['episodes'] = self.get_episodes_by_series(show_id)

        # Add show to list
        self.shows.append(show)

        # Fetch poster
        poster = self._get_with_token(f'{IMAGE_BASE_URL}/{show["poster"]}')
        with open(f'{DATA_DIR}/{show_id}.jpg', 'wb') as fp:
            fp.write(poster.content)

    def dump_shows(self):
        with open(f'{DATA_DIR}/shows.json', 'w') as fp:
            json.dump(self.shows, fp, indent=4)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='tv-shows-kg',
                                     usage=('./fetcher.py <TV SHOWS FILE>'
                                     '--username <TVDB username> --user-key'
                                     '<TVDB user key> --api-key <TVDB API key>'
                                     '--mapping-file <RML MAPPING FILE> --jar'
                                     '<RMLMAPPER JAR>'),
                                     description='Create a Knowledge Graph from your TV show library with the TVDB API.')
    parser.add_argument('shows',
                        type=str,
                        help='A JSON file with an array of TVDB IDs')
    parser.add_argument('--username',
                        type=str,
                        required=True,
                        help='TVDB username')
    parser.add_argument('--user-key',
                        type=str,
                        required=True,
                        help='TVDB user key')
    parser.add_argument('--api-key',
                        type=str,
                        required=True,
                        help='TVDB API key')
    parser.add_argument('--mapping-file',
                        type=str,
                        required=True,
                        help='Path to RML mapping rules as a Turtle file')
    parser.add_argument('--jar',
                        type=str,
                        required=True,
                        help='Path to the rmlmapper-java JAR')
    args = parser.parse_args()

    with open(args.shows, 'r') as fp:
        shows = json.load(fp)

    # Fetch and map
    fetcher = Fetcher(args.username,
                      args.user_key,
                      args.api_key)
    for show in shows:
        print(f'TV show: {show["name"]} ({show["id"]})')
        fetcher.save_show(show['id'])

    fetcher.dump_shows()

    print('Executing mappings...')
    mapper = Mapper(args.mapping_file,
                    f'{DATA_DIR}/shows.ttl',
                    args.jar)
    mapper.execute()
