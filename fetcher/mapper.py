#!/usr/bin/env python

import os
import subprocess

class Mapper:
    def __init__(self, mapping_file, output_file, mapper_jar):
        self._mapping_file = mapping_file
        self._output_file = output_file
        self._mapper_jar = mapper_jar

    def execute(self):
        subprocess.call(['java', '-jar', self._mapper_jar] \
                        + ['-m', self._mapping_file] \
                        + ['-o', self._output_file] \
                        + ['-s', 'turtle'])


if __name__ == '__main__':
    m = Mapper('mappings.rml.ttl', 'data.ttl')
    m.execute()
